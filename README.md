A simple interfance to help track and clean up .NET reference in a way that keeps your block diagram looking clean.

# .NET Cleanup

Properly cleaning up .NET references can quickly get out of hand. Typical .NET code creates a lot of references that are only used for a short period of time.

This toolkit help developers keep track of these reference and easily close all of them only once they're no longer need.

### Usage

Call `Reference Cleanup.lvclass:Reference Cleanup.vi` to initialize the reference counter.

Next, when new .NET reference is crated, add it to the counter by calling `Reference Cleanup.lvclass:Add Reference.vim`

Finally, when you're done with all of the references, call `Reference Cleanup.lvclass:Cleanup.vi`

### How References are tracked

References are tracked using the VI that created them. When the `Reference Cleanup` object is created, a named queue is created based on the calling VI's name. Next when `Add Reference` is called the call chain is searched for an existing cleanup queue, and the reference is added to it.

So typically a VI will initialize the `Reference Cleanup` at the start of execution. Perform all of it's .NET related tasks (and just add reference as they're created) then finally close all references at the end of execution.

Any references that implements the .NET `IDisposable` interface will be disposed of before closing.

## Contributing

See [Contributing.md](CONTRIBUTING.md) for information on how to submit pull requests. Bugs can be reported using the repositories issue tracker.

#### _This package is implemented with LabVIEW 2017_
